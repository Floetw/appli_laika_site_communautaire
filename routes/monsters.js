const express = require('express');
const monsters = require('../monsters');
const monsterRouter = express.Router();

monsterRouter.get('/all', function(request, response){
    response.render('index', {monsters:monsters});
})

monsterRouter.get('/:id', function(request, response){
    const monster = monsters[request.params.id -1];
    response.render('monstre', {monster : monster});
})



module.exports = monsterRouter;