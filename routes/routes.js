var express = require('express');
var path = require('path');

// Création de notre router
var router = express.Router();


// On appelle les monsters
var monsters = require('../monsters');

// Route vers la Home Page
router.get('/', function(request, response) {
    response.render('home');
})
router.get('/communaute', function(request, response){
    var users = [
        {name:"jarrod", email:"jarrod@me", avatar:"./img/jarrod.jpg"},
        {name:"edwige", email:"edwige@me", avatar:"./img/edwige.jpg"},
        {name:"timie", email:"timie@me", avatar:"./img/timie.jpg"},
    ]
    response.render('communaute', {users: users});
})
router.get('/forum', function(request, response) {
    response.render('forum');
})
router.get('/contact', function(request, response) {
    response.render('contact');
})
router.get('/create_monster', function(request, response) {
    response.render('new_monster');
})
router.post('/create_monster', function(request, response) {
    monsters.push({
        name:request.body.name,
        desc:request.body.desc,
        level:parseInt(request.body.level, 10)
    })
    console.log(request.query);
    response.send('ok');
})

module.exports = router;