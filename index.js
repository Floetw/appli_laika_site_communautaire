// Appeler les dépendances
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 5000;
var volleyball = require('volleyball');
var router = require('./routes/routes');
var monsterRouter = require('./routes/monsters')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'pug');
app.use(volleyball);
app.use('/', router);
app.use('/monstre', monsterRouter);


app.listen(port, function() {
    console.log('[Application OK]');
});

